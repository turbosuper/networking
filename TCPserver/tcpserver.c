/*
 * =====================================================================================
 *
 *       Filename:  tcpserver.c
 *
 * =====================================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <sys/select.h>

#define PORT 44444
#define MAXLINE 1024
#define MAX_CLIENTS 10
#define MAX_BUFFER 1024

void failure(char* message){
	perror(message);
	exit(1);
	}

int main(){

	int lfd; /* Listen File Deskriptor */
	int nfd[MAX_CLIENTS]; /* Array mit FD fuer Clients */
	struct sockaddr_in server_address;
	struct sockaddr recieve_address;
	struct sockaddr_in client_address;
	socklen_t client_add_len;
	char client_msg[MAX_BUFFER];
	ssize_t msg_len;
	size_t msg_count = MAX_BUFFER;
	fd_set  rfds_orig, rfds_read;
	struct timeval tv;
	int retval;
	FD_ZERO(&rfds_orig);
	int fdmax;
	ssize_t sentbytes;
	int i = 0;
	ssize_t welcome_msg_len;
	ssize_t client_ip_len;
	int recieve_data_len;
	socklen_t recieve_add_len;
	ssize_t new_line_len;
	char welcome_msg[] = "\n[SERVER] Welcome to the chat! Your IP Adress is: " ;
	welcome_msg_len = sizeof(welcome_msg);
	char new_line = '\n';


	/* Listen Socket Erstellen */
	if ( (lfd = socket(AF_INET, SOCK_STREAM, 0)) == -1 ) { 
			failure("listen socket initialising");
		}
	printf("Listen socket: %d\n", lfd);

	/* Server Adress bestimmen */
	server_address.sin_family = AF_INET;  //Domain
	server_address.sin_addr.s_addr = INADDR_ANY; /* Jede kann sich mit dieser Addresse verbinden */
	server_address.sin_port = htons(PORT);

	
	/* Verbinden der Socket zu einer Spezifischer Adresse und Port */
	if( bind(lfd, (struct sockaddr*) &server_address, sizeof(server_address)) == -1){
			failure("bind");
		}

	if (listen(lfd, 3)== -1){ /* Die 3 hier entspricht die erlaubte Stau entspricht nicht die MAXCLIENTs */
		failure("listen");
		}	
	fdmax = lfd;
	FD_SET(lfd, &rfds_orig); /* Listen Socket zur Lese File Deskriptoren hinzufuegen */	
	
	while(1){

		/* Zeit und Dateideskriptoren fuer Select bestimmen */
		tv.tv_sec = 9;
		tv.tv_usec = 0;
		rfds_read = rfds_orig; /* Die Kopie von RFDS erstellen */
		retval = select(fdmax +1 , &rfds_read, NULL, NULL, &tv);/* Wenn etwas an FD passiert  */

		if (retval != 0){ 		
			for (int k =0; k<=fdmax; k++) { /* Durch alle FD interrieren */
				if((FD_ISSET(k, &rfds_read))==1) { /* k wird jetzt einen FD wo etwas passiert */
					if(k == lfd){	
						/* wenn es bei Listen Socket passiert, dann soll es :
						 * -accept - neuen FD erstellen,
						 * -eine Wilkommens Nachricht an der Client senden
						 * -diese socket der neuen Client in FD_SET hinzufuegen
						 * */
						
						client_add_len = sizeof(client_address); 
						client_ip_len = strlen(inet_ntoa(client_address.sin_addr));
						new_line_len = sizeof(new_line);
						nfd[i] = accept(lfd,(struct sockaddr*) &client_address , &client_add_len);
						if ((sentbytes = send(nfd[i], &welcome_msg, welcome_msg_len, 0)) == -1){ 
							failure("send");
							}
						if ((sentbytes = send(nfd[i], inet_ntoa(client_address.sin_addr), client_ip_len, 0)) == -1){ 
							failure("send");
							}
						if ((sentbytes = send(nfd[i], &new_line, new_line_len, 0)) == -1){ 
							failure("send");
							}
						FD_SET(nfd[i], &rfds_orig); /* Neuer client an zu der FD Set hinzufuegen */
						if (fdmax<nfd[i]) { /* Falls Neue Client, FDMAX erhoehen */
							fdmax = nfd[i];
							}
						printf("New Client from %s on socket %d\n", inet_ntoa(client_address.sin_addr),nfd[i]);
						/*Die Maximale Anzahl der Clients beobachten */
						if (i< MAX_CLIENTS) i++;/* bei nachster client einen neuen socket erstellen */
					       	else {
							failure("Too many clients\n");
								break;		
						}
					}	
					else { 
						/* Keine neuer client, dh alte Client meldet sich, dann soll es
						* -von dieser Client Nachricht empfangen
						* -an alle andere senden	*/
						
						recieve_add_len = sizeof(recieve_address); 
						if ( (msg_len = recvfrom(k, &client_msg, msg_count, 0, &recieve_address, &recieve_add_len)) <= 0){
							if (msg_len ==-1 ) failure("recv");
							if (msg_len == 0) {
								printf("Client on the socket: %d hang up\n", nfd[k]);
								close(k);
								FD_CLR(k, &rfds_orig); /* wenn Client is tot, dann es von FDSEt entfernen */
								}
							}	
						else{ 
							/* Wenn Nachricht erfolgreich empfangen ist */
							recieve_data_len = sizeof(recieve_address.sa_data);
							client_msg[msg_len] ='\0';
						       	/*Durch die Arrey mit FD iteriren, an alle absenden */
							for( int j=0; j <= fdmax; j++){	
								/* Senden, wenn es eine FD der Clients ist.... */
								if(FD_ISSET(j, &rfds_orig)){ 
								 	/* ...aber weder Server noch Client selbst */
								        if(j != lfd && j != k){
									/* 	Eigene Adresse an der Client senden	
										if ((sentbytes = send(j, recieve_address.sa_data , recieve_data_len, 0)) == -1){ 
										failure("send");
										}*/
										if ((sentbytes = send(j, &client_msg, msg_len+1, 0)) == -1){ 
										failure("send");
										}
									  }
									}
								}
							}
						}
					}
				}
			}
		else break;
		}

	close(lfd);
	return(0);
}	
 
 
