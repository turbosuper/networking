/*
 * =====================================================================================
 *q
 *       Filename:  udpserver.c
 *
 *
 * =====================================================================================
 */



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/select.h>

#define PORT 4030
#define MAXLINE 1024

void failure(char* message){
	perror(message);
	exit(1);
	}


int main(){

/* Aus der man page:
 * struct sockaddr_in{
		sa_family_t sin_family;
		in_port_t sin_port;
		stuct in_assr sin_addr;
		}  */
	
	int server_socket; //socket file descriptor
	struct sockaddr_in server_address;
	char buffer[MAXLINE];
	char *server_messagge = "Hier ist der Server";
	socklen_t len;
      	int n;


	/* Server Socket erstellen */
	if((server_socket = socket( AF_INET, SOCK_DGRAM, 0)) == -1) {  /* Domain, Datagram, 0 fuer das standard Protokol */
		failure("socket");
		}
	
	printf("server socket no: %d\n", server_socket);
	memset(&server_address, 0 ,sizeof(server_address)); /*Fuellt die anzahl der bytes des gegebenes Adresse mit 0 */
	
	/* Server Adress bestimmen */
	server_address.sin_family = AF_INET;  //Domain
	server_address.sin_addr.s_addr = INADDR_ANY; 
	server_address.sin_port = htons(PORT);
	
	/* Verbinden der Socket zu einer Spezifischer Adresse und Port */
	if( bind(server_socket, (struct sockaddr*) &server_address, sizeof(server_address)) == -1){
		/* Argumente: socket fd; struct mit spezieferte Adresse; Groesse von dieser Struct 
		 * Bind bedeutet dass wir den Socket mit das Sistem verbinden*/
		failure("bind");
		}
	
	/* Warten bis einer Paket kommt */
//	while(1){	
	printf("Warten.....\n");
	fflush(stdout);

	if( (n = recvfrom(server_socket, (char *)buffer, MAXLINE,
		0, (struct sockaddr *) &server_address, &len)) == -1){
		/*Argumente: socket fd; Puffer, wo der packet gespiechert sein soll; Groesse dieser Puffer;
			     Flags fuer Socket Beziehung; Struct mit Adresse der Quelle; Groesse dieser Struct
		Recieve beduetet das Sistem bekommt von dieser Socket die Daten*/
		failure("recvfrom");
		}
	
	buffer[n] = '\0'; //letzte Char als neuline char stellen
	printf("client sagt: %s\n", buffer);

	/* Nach dem Empfand etwas zurueck am Client senden */
	if((n = sendto(server_socket, (const char*)server_messagge, strlen(server_messagge), 
		0,   (const struct sockaddr *) &server_address, len)) == -1){
		/*Argumente: socket fd; Puffer mit die Daten zu absenden; Groesse dieser Puffer;
			     Flags fuer socket handlung; Struct mit Adresse der Empfaenger; Groesse dieser Struct */
		failure("sendto");
		}
	
	buffer[n] = '\0'; //letzte Char als neuline char stellen
	printf("Mitteilung gesendet\n");
//		}
	
	close(server_socket);
	return 0;
}


 	/* Zuhoeren 
	listen(server_socket, 5); //5- Anzahl der erlaubte Verbindungen in Warteschlange

	* Akzepitieren einer Verbinudung, und spiecher der client socket 
	client_socket = accept(server_socket, NULL, NULL); //die beiden NULLen weil es geht um eine lokale verbingung

	* Daten entsendun zu der Client 
	send(client_socket, server_message, sizeof(server_message), 0);
	close(server_socket);

	  */


