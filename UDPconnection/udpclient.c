/*
 * =====================================================================================
 *
 *       Filename:  updclient.c
 *
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <sys/select.h>

#define PORT 4030
#define MAXLINE 1024

void failure(char* message){
	perror(message);
	exit(1);
	}

int main(int argc, char *argv[]){
	int sfd; /* FD fuer das Socket - so werden wir es spaeter aufrufen koennen */
	char buffer[MAXLINE];
	char *client_messagge = "Hallo, Ich bin der Client";
	struct sockaddr_in server_address;
	socklen_t len; /* Groesse der socket struct darf kein int sein */

	/* Die Variebel unten sind fuer Richtge Einstellung der SELECT funktion */
	fd_set rfds; /* Wir werden die FD auf lesen beobachten */
	fd_set wfds; /* Wir werden die FD auf lesen beobachten */
	int retval; /* Ruckgabewert fuer das select Funktion */
	struct timeval tv; /* Einer Struct die hat die Zeit Werte fuer select gespiechert */
	FD_ZERO(&rfds); /* ganzen Set auf Null Stellen */
	int anzahl = 3;
	int sendret;

	/* Pruefen ob Addressei bei Nutzer gegeben war */
	if (argc != 2){
		fprintf(stderr, "%s: Fehler, brauche Adresse mit Punkten und in Ausfruche zeichen zb \"127.0.0.1\"\n", argv[0]);
		exit(EXIT_FAILURE);
		}	


	/* Erst die Socket initialisieren  */
	if ( (sfd = socket( AF_INET, SOCK_DGRAM, 0)) == -1){  /* AF_INET ist der Domain
       						SOC_DGRAM ist einen Typ der Verbindung - hier Datagram
						0 iste fuer das standard Protokol (default)*/
		failure("socket");
		}

	
	printf("Client socket no %d\n", sfd);


	memset(&server_address, 0, sizeof(server_address));

	/* Bestimmen einen Address zu verbinden */

	server_address.sin_family = AF_INET; /* Des ist der Domain */
	server_address.sin_port = htons(PORT); /* htons fuer dasKonvertierung des Nummers des Ports von Little Endian notation */
	server_address.sin_addr.s_addr = INADDR_ANY; /* sin_addr ist einer struct INNERHALB der sockaddr_in sturct
						     * INADDR_ANY ist die Abkuerzung von 0.0.0.0 */

	/* Die gegebene Adresse aus der Kommando Zeile in einer char umwandeln	*/
	if (inet_aton(argv[1], &server_address.sin_addr) == 0){
		fprintf(stderr, "Invalid address\n");
		exit(EXIT_FAILURE);
		}
	
	printf("Die gegebene adresse ist %s\n", inet_ntoa(server_address.sin_addr));


	/* 3 mal An der Server Nachricht senden versuchen */
	while(anzahl){
		if(sendto(sfd, (const char *)client_messagge, strlen(client_messagge), 
				0, (const struct sockaddr *) &server_address, sizeof(server_address)) == -1){;
				/* Beschriebung dieser Funktion  in der udpserver.c */
			failure("sendto");
			}

		FD_SET(sfd, &rfds); /* SELECT: Wir stellen FD No 3 in der Set -es wird beobachtet */
		tv.tv_sec = 1; /* 1 Sekunde Warten */
		tv.tv_usec = 0;
		retval = select(sfd+1, &rfds, NULL, NULL, &tv); /* sfd+1 - Anzahl der FD die beobachtet sein solle
							     &rfds - FD nach lesen beobachtet sein sollen
							     keine FD nach Schreiben oder Ausnahmen beobachtet sein soll,
							     &tv zeigt auf der Zeit*/
		if(retval == -1) perror ("select()");

		if (retval == 0) anzahl--;
		if (retval != 0) break;
		}


	if (anzahl == 0) failure("Absenden nicht erfolgreich");

	printf("Der Mitteilung ist gesendet geworden\n");

	/* Warten und eine Nachricht von der Server bekommen */

	if( recvfrom(sfd, (char *)buffer, MAXLINE, 0,  (struct sockaddr * restrict) &server_address, &len) == -1){
			/* Beschriebung dieser Funktion in der udpserver.c */
		failure("recvfrom");
		}
//		buffer[n] = '\0';
	printf("Server antwrotet: %s\n", buffer);
			
//	else printf("Server hat innerhal von 3 Sekunden nicht genatwortet\n");

	close(sfd);

	return 0;
}



	/* Verbinden  	
	connection_status = connect(sfd, (struct sockaddr *) &server_address, sizeof(server_address));	//Verbindet der Socket mit eine Adresse
	if (connection_status == -1){
		printf("connection failed\n");
	}

	* Daten empfangen *
	
	recv(sfd, &server_response, sizeof(server_response), 0); //Empfaengt die Nachrich von der socket, speichert in einen String

	* Ausgabe der Bekommene Daten *
	printf("Der Server hat das gesendet: \n %s\n", server_response);

	* Der Socket schliessen */

