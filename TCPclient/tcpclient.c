/*
 * =====================================================================================
 *
 *       Filename:  tcpclient.c
 *
 *
 * =====================================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <sys/select.h>
#include <readline/readline.h>
#include <readline/history.h>

#define PORT 44444
#define MAXLINE 1024
#define MAX_CLIENTS 10
#define MAX_BUFFER 1024

void failure(char* message){
	perror(message);
	exit(1);
	}

int main(){
	int cfd;
	char buf[MAX_BUFFER];
	char client_messagge[MAXLINE];
	char client_msg[MAX_BUFFER];
	char *client_prompt = "Say what: ";
	char *welcome_messagge = "Hallo, ich bin der Client";
	ssize_t sentbytes;
	ssize_t recvbytes;
	fd_set rfds_orig;
	fd_set rfds_copy;
	struct timeval tv;
	int retval;

	FD_ZERO(&rfds_orig);
//	FD_ZERO(&rfds);
//	FD_SET(0, &rfds);

	struct sockaddr_in client_address;
	struct sockaddr_in server_address;
	socklen_t client_address_len;

	server_address.sin_addr.s_addr = inet_addr("127.0.0.1");
	server_address.sin_port = htons(PORT);
	server_address.sin_family = AF_INET;

	if ( (cfd = socket(AF_INET, SOCK_STREAM, 0)) == -1 ) { 
			failure("client socket initialising");
		}
	printf("Client socket: %d\n", cfd);
	
	if ( (connect(cfd,(struct sockaddr *) &server_address, sizeof(server_address))) < 0 ) { 
			failure("connect");
			}

	FD_SET(cfd, &rfds_orig);
	FD_SET(fileno(stdin), &rfds_orig);

	if ((sentbytes = send(cfd, (const char *)welcome_messagge, strlen(client_messagge)+1,0)) < 0){
			failure("send");
			}
	while(1){
//		printf("Your message: ");

	/*  	if ((retval = select(cfd+1, &rfds,NULL, NULL, &tv)) == -1){
			failure("select");
		}*/
		rfds_copy = rfds_orig;
		tv.tv_sec =10;
		tv.tv_usec = 0;
		printf(":");
		retval = select(cfd+1, &rfds_copy, NULL, NULL, &tv);
		if (retval != 0){ /* wenn etwas auf RFDS passiert */
			if (FD_ISSET(fileno(stdin), &rfds_copy)==1){ /* wenn etwas auf der STIO passiert */
				scanf("%s", client_messagge);
				
				if ((sentbytes = send(cfd, (const char *)client_messagge, strlen(client_messagge),0)) < 0){
					failure("send");
					}
				}

			if (FD_ISSET(cfd, &rfds_copy)){ /* wenn etwas auf der Socket passiert */
				if ((recvbytes = recv(cfd, buf, MAX_BUFFER,0)) < 0){
					failure("recv");
					}
				if (recvbytes ==0){
					printf("Server is not there anymore \n");
					break;}
				printf("Message Recieved:   %*s \n", recvbytes, buf);
				}
		}
		if (retval ==0)	break;
		}
	close(cfd);
	return EXIT_SUCCESS;
}
/* Ein SELECT fuer Tastatur, ein fuer read() */
